﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowardDirection : MonoBehaviour
{
    public Vector3 direction = new Vector3(1, 0, 0);
    float movementSpeed = 5;

    void Start() 
    {

    }

    // Physics code inside update function || Movement code inside lateupdate function
    void LateUpdate()
    {
        transform.Translate(direction.normalized * movementSpeed * Time.deltaTime);
    }
}
