﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogMovement : MonoBehaviour
{
    public float speed = 7;
    float dirX;
    float dirZ;

    // Update is called once per frame
    void LateUpdate()
    {
        dirX = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        dirZ = Input.GetAxis("Vertical") * speed * Time.deltaTime;

        transform.position = new Vector3(transform.position.x + dirX, transform.position.y, transform.position.z + dirZ);
    }
}
