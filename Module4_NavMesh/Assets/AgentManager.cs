﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentManager : MonoBehaviour
{
    public GameObject guide;
    GameObject[] agents;
    
    // Start is called before the first frame update
    void Start()
    {
        agents = GameObject.FindGameObjectsWithTag("AI");
    }

    // Update is called once per frame
    void LateUpdate()
    {
         foreach(GameObject ai in agents)
         {
             ai.GetComponent<AIController>().agent.SetDestination(guide.transform.position);
         }
        // if(Input.GetMouseButtonDown(0))
        // {
        //     RaycastHit hit;

        //     if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000))
        //     {
        //         foreach(GameObject ai in agents)
        //         {
        //             ai.GetComponent<AIController>().agent.SetDestination(hit.point);
        //         }
        //     }
        // }
    }
}
