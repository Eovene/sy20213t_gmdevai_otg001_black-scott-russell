﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPosition : MonoBehaviour
{
    public GameObject player;
    public Transform startPos;

    void start()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        player.transform.position = startPos.transform.position;
        Debug.Log("Trigger");
    }
}
