﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : TankStats
{
    Animator animator;
    private bool hasFled = false;

    void Start()
    {
        animator = this.GetComponent<Animator>();
    }

    public override void TakeDamage(float damage)
    {
        health -= damage;

        if(health <= 25 && hasFled == false)
        {
            hasFled = true;
            animator.SetTrigger("IsDying");
        }
        else{
            animator.ResetTrigger("IsDying");
        }

        if(health <= 0)
        {
            Die();
        }
    }
}
