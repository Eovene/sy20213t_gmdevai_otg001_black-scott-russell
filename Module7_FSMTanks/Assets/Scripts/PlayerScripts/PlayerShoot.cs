﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public GameObject bullet;
    public GameObject turret;
    public int testNumber = 0;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown("space"))
        {
            GameObject b = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
            Physics.IgnoreCollision(b.GetComponent<Collider>(), this.GetComponent<BoxCollider>());
            b.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 500);
        }
    }
}
