﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleMovement : MonoBehaviour
{    
    public Transform goal;
    public float speed = 0;
    public float rotSpeed = 3;
    public float acceleration = 5;
    public float deceleration = 5;
    public float minSpeed = 0;
    public float maxSpeed = 10;

    public float breakAngle = 20;

    void LateUpdate()
    {
        //Vector3 lookAtGoal = new Vector3(goal.position.x, this.transform.position.y, goal.position.z);
        Vector3 lookAtGoal = new Vector3(goal.position.x, goal.position.y, goal.position.z);
        Vector3 direction = lookAtGoal - transform.position;
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, 
                                                    Quaternion.LookRotation(direction), 
                                                    rotSpeed * Time.deltaTime);
        //speed = Mathf.Clamp(speed + acceleration * Time.deltaTime, minSpeed, maxSpeed);
        
        if(Vector3.Angle(goal.forward, this.transform.forward) > breakAngle && speed > 2)
        {
            speed = Mathf.Clamp(speed - (deceleration * Time.deltaTime), minSpeed, maxSpeed);
        }
        else
        {
            speed = Mathf.Clamp(speed + (acceleration * Time.deltaTime), minSpeed, maxSpeed);
        }
        transform.Translate(0, 0, speed);
    }
}
